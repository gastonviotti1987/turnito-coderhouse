import Vue from 'vue'
import Router from 'vue-router'
import Reservas_index from '@/components/Reservas_index'
import inicio from '@/components/inicio'
import luser from '@/components/luser'
import clientes from '@/components/clientes'
import loginsoc from '@/components/login-soc'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Reservas_index',
      name: 'Reservas_index',
      component: Reservas_index
    },
    {
      path: '/inicio',
      name: 'inicio',
      component: inicio
    },
    {
      path: '/luser',
      name: 'luser',
      component: luser
    },
    {
      path: '/clientes',
      name: 'clientes',
      component: clientes
    },
    {
      path: '/loginsoc',
      name: 'loginsoc',
      component: loginsoc
    },

    { path: '/', redirect: '/inicio' }
  ]
})




