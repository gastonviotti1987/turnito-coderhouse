import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'

// P/ pwa tambien ver vue.config.js
// GGG-PWA-------------------------v
// import './registerServiceWorker'
// GGG-PWA--------------------------^

// GGG ----------------------------v
import store from './store'
// GGG ----------------------------^


Vue.config.productionTip = false

new Vue({
  router,
  store, // GGG Si trabajamos con vuex
  vuetify,
  render: h => h(App),
}).$mount('#app')

